1. background knowledge about "JWT": https://en.wikipedia.org/wiki/JSON_Web_Token
2. download it from https://github.com/Thalhammer/jwt-cpp
   While downloaded it, its latest tag is "v0.3.1"
3. It only use header file. Do not pre-compile to generate library file
4. You can compile example with following command: g++ -std=c++11 -I$CODEBASEDIR/xcipio/jwt-cpp/include -I/pbc/libuild/GIT_3prty_LI/3rdparty/LINUX/boost_1_57_0/include example/print-claims.cpp -L/pbc/libuild/GIT_3prty_LI/3rdparty/LINUX/openssl-1.0.2/lib -lcrypto -lssl
